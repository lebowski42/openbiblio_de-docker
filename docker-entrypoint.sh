#!/bin/bash
set -e
# Replace DB-settings with env
envsubst < /database_constants.php.tmpl > /var/www/html/database_constants.php


service php7.2-fpm start && nginx
